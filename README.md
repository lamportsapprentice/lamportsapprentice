# Hi, I am Hannes!

I am part of the [Gitlab Incubation Engineering](https://about.gitlab.com/handbook/engineering/incubation/) department, working on
[real-time editing](https://about.gitlab.com/handbook/engineering/incubation/real-time-collaboration/). My work includes playing around with [CRDTs](https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type), in particular the ones implemented by [Y.js](https://docs.yjs.dev/) and its [Ruby port](https://github.com/y-crdt/yrb).

Previously I co-founded a search & discovery organization, and led a team that built the next generation of information retrieval systems at [Shopify](https://shopify.com/). Today, this platform powers all the discovery features (search, recommender systems, …) built by dozens search engineers, relevance engineers, data engineers and data scientists at Shopify.

My [partner](https://neele.codes/) and I, spend our free time [running](https://www.strava.com/athletes/20638565), [cooking](https://github.com/spurtli/foodies), learning new things like
[data science & machine learning](https://www.datacamp.com/) and [building mechanical keyboards](https://github.com/spurtli/mech). Together, we
have built [Spurtli](https://spurt.li), a social hub for athletes, [neele.cooks](https://neelecooks.com), an app to create and
share tasty and fast to cook dishes, and organized the UX and web development
conference [.concat()](https://conc.at/).

For the last 20+ years I have built games, [platforms](https://www.shopify.com/), [services](https://www.redbullcontentpool.com/) and teams for a
multitude of companies such as [Shopify](https://www.shopify.com/),
[Red Bull](https://www.redbullcontentpool.com/),
[MED-EL](https://s3.medel.com/pdf/US/Continents-and-Oceans.pdf) and
[Wiberg](https://www.redbullcontentpool.com/).
My contributions include, but are not limited to, technical decision making,
applied R&D, mentoring and leading teams.

I am an adjunct lecturer, teaching undergrad and grad students at the [University
of Applied Sciences Salzburg](https://www.fh-salzburg.ac.at/en/) MultiMediaTechnology department. Occasionally you
can also listen to me, when I am [speaking](http://localhost:8000/talks) about web development and academia at
conferences and when I participate in community events such as [Hackathons](https://www.game-jam.at/),
[Barcamps](https://barcamp-sbg.at/) & [Meetups](https://www.meetup.com/en-AU/salzburgwebdev/).
